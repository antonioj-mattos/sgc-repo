from django.conf.urls import url

from . import views

urlpatterns = [
    url(
        regex=r'^(?P<identification>[\w.@+-]+)/$',
        view=views.enroll,
        name='enroll'
    ),
]
