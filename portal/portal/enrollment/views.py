from django.shortcuts import render

from portal.taskapp.celery import send_enrollment_message


def enroll(request, identification):
    send_enrollment_message(identification)
    return render(request, "enrollment/details.html")
