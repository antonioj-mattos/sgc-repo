#!/bin/sh

set -o errexit
set -o pipefail
set -o nounset


celery -A admin.taskapp beat -l INFO
