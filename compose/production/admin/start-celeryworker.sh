#!/bin/sh

set -o errexit
set -o pipefail
set -o nounset


celery -A admin.taskapp worker -l INFO
