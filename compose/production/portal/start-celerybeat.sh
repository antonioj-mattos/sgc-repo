#!/bin/sh

set -o errexit
set -o pipefail
set -o nounset


celery -A portal.taskapp beat -l INFO
