#!/bin/sh

set -o errexit
set -o pipefail
set -o nounset


celery -A portal.taskapp worker -l INFO
