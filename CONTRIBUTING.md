# Guia de colaboração

Esta *guideline* define o processo de criação ou modificação de outras *guidelines*, bem como o processo de modificação desta.

## Criação ou modificação de *guidelines*

A proposição de criação ou modificação de uma *guideline*, segue as seguintes regras:
1. Cada aspecto diferente do projeto, deve estar contido em uma única e distinta *guideline*. Dessa forma, para cada *guideline* deve exitir um arquivo .md (*markdown*), o qual deve versar sobre um único aspecto do projeto;
2. Cada proposta de alteração deve conter alterações exclusivamente para uma única *guideline*
3. O participante do projeto que deseja criar/alterar uma *guideline* deve criar/alterar o arquivo correspondente e enviar um MR (*Merge Request*) para o *branch* **master**;
4. As propostas de criação/alteração enviadas serão avaliadas, de acordo com as seguintes diretrizes:
    1. Os MRs enviados ficarão abertos para discusão e votação até que uma das seguintes situações ocorra:
        1. Seja passado um período de 7 dias, a contra do horário exato da abertura do MR; ou
        2. 100% dos colaboradores ativos tenham expressado o seu voto de aprovação ou rejeição da proposta.
    2. Durante o período de avaliação da proposta, todos os colaboradores ativos (ver seção [Colaboradores Ativos](CONTRIBUTING.md#Colaboradores) poderão comentar e recomendar ajustes no MR aberto.
    3. Quando o colaborador considerar que a discussão do MR já foi exaurida, este deverá emitir o seu voto para aprovação ou rejeição da proposta, exclulsivamente através do recurso de votação de *merge requests* do GitiLab.
    4. Encerrado o período de discussão, seja pelo disposto no item 4.1.1 ou no 4.1.2, o MR será aceito se obtiver uma quantidade de votos positivos igual ou superior a 2/3 dos votos, ou fechado, caso contrário.
        1. Excepcionalmente, para alterações no [README](README.md) e nesse Guia de Colaboração, o MR só será aceito se receber uma quantidade de votos favoráveis igual ou superior a 2/3 do total de membros ativos.
    5. Modificações pequenas em uma *guideline* já existente que visam apenas corrigir pequenos detalhes (ex.: *typo*, formatação, adição de nota explicativa, etc.) poderão ser feitas diretamente, sem a necessidade de passar pelo processo descrito acima, desde que a proposta não contenha qualquer alteração substancial nas definições ou regras expressas na *guideline*.
 

Colaboradores
-------------

Abaixo são encontradas as listas de colaboradores ativos e colaboradores antigos. A adição de novos colaboradores e o desligamento de colaboradores ativos seguem o processo de modificação deste Guia de Colaboração, descritos acima.

### Colaboradores ativos

Movido para [CONTRIBUIDORES](CONTRIBUTORS.txt).
 
### Colaboradores antigos

Não há colaboradores antigos atualmente.
 