SGC
===

Sistema para gerenciamento do ciclo de vida de concursos e vestibulares no IFSP.

.. image:: https://img.shields.io/badge/built%20with-Cookiecutter%20Django-ff69b4.svg
     :target: https://github.com/pydanny/cookiecutter-django/
     :alt: Construído com Cookiecutter Django


:Licença: MIT


Configurações
-------------

Movido para settings_.

.. _settings: http://cookiecutter-django.readthedocs.io/en/latest/settings.html

Comandos básicos
----------------

Configurando seus usuários
^^^^^^^^^^^^^^^^^^^^^^^^^^

* Para criar uma **conta de usuário normal**, basta inscrever-se e preencher o formulário. Depois de enviá-lo, você verá uma página "Verifique seu endereço de e-mail". Vá para o seu console para ver uma mensagem de verificação de email simulada. Copie o link para o seu navegador. Agora, o e-mail do usuário deve estar verificado e pronto para uso.

* Para criar uma **conta de superusuário**, use este comando ::

    $ python manage.py createsuperuser

Por conveniência, você pode manter seu usuário normal logado no Chrome e seu superusuário logado no Firefox (ou similar), para que você possa ver como o site se comporta para ambos os tipos de usuários.

Cobertura de teste
^^^^^^^^^^^^^^^^^^

Para executar os testes, verifique a cobertura do teste e gere um relatório de cobertura em HTML::

    $ coverage run manage.py test
    $ coverage html
    $ open htmlcov/index.html

Executando testes com py.test
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

  $ py.test

Recarregamento automático e compilação Sass CSS
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Movido para `Live reloading e compilação Sass CSS`_.

.. _`Live reloading e compilação Sass CSS`: http://cookiecutter-django.readthedocs.io/en/latest/live-reloading-and-sass-compilation.html



Celery
^^^^^^

Este aplicativo vem com Celery.

Para executar um worker:

.. code-block:: bash

    cd peel
    celery -A peel.taskapp worker -l info

Por favor, note: Para que a magia de importação de Celery funcione, é importante *onde* os comandos do celery são executados. Se você estiver na mesma pasta com *manage.py*, você deve estar ficar bem.





Sentry
^^^^^^

O Sentry é um serviço de agregação de log de erros. Para instalação consulte https://docs.sentry.io/server/installation/.
O sistema é configurado com padrões razoáveis, incluindo 404 logging e integração com o aplicativo WSGI.

Você deve definir a URL do DSN em produção.


Implantação
-----------

Os seguintes detalhes sobre como implantar esta aplicação.



Docker
^^^^^^

Veja detalhes `cookiecutter-django Documentação Docker`_.

.. _`cookiecutter-django Documentação Docker`: http://cookiecutter-django.readthedocs.io/en/latest/deployment-with-docker.html



